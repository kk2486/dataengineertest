
name := "dataengineertest"

version := "0.1"

scalaVersion := "2.12.5"

val sparkVersion = "3.1.1"

val testLibs = Seq("org.scalatest" %% "scalatest" % "3.2.2" % Test
  , "org.scalacheck" %% "scalacheck" % "1.14.3" % Test)

libraryDependencies ++= Seq("org.apache.spark" %% "spark-core" % sparkVersion % Provided
  , "org.apache.spark" %% "spark-sql" % sparkVersion % Provided) ++ testLibs

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

assemblyMergeStrategy in assembly := {
  case PathList("javax", "servlet", xs @ _*) => MergeStrategy.last
  case PathList("javax", "activation", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", xs @ _*) => MergeStrategy.last
  case PathList("com", "google", xs @ _*) => MergeStrategy.last
  case PathList("com", "esotericsoftware", xs @ _*) => MergeStrategy.last
  case PathList("com", "codahale", xs @ _*) => MergeStrategy.last
  case PathList("com", "yammer", xs @ _*) => MergeStrategy.last
  case "about.html" => MergeStrategy.rename
  case "META-INF/ECLIPSEF.RSA" => MergeStrategy.last
  case "META-INF/mailcap" => MergeStrategy.last
  case "META-INF/mimetypes.default" => MergeStrategy.last
  case "plugin.properties" => MergeStrategy.last
  case "log4j.properties" => MergeStrategy.last
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
