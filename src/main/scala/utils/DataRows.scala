package utils

case class DataRows(
                     rowId: String,
                     dataRow: Map[String, Int]
                   )