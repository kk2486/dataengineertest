package utils

case class Consumption(uniqueKey: String,
                       division: String,
                       gender: String,
                       category: String,
                       channel: String,
                       year: String,
                       dataRows: Seq[DataRows])