package salesconsumption

import org.apache.spark.sql.{DataFrame, SparkSession}

// CSV Reader object

object CSVReader {

  // constants just below are considered.
  val sourceMap = Map("header" -> "true", "inferSchema" -> "true")

  def read(inputLoc: String, filename: String, spark: SparkSession): DataFrame = {

    // TODO: Can be improved by checking the input paths exists or not and error frame work around it.
    spark.read.options(sourceMap).csv(inputLoc + filename)

  }


}
