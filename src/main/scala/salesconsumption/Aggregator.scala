package salesconsumption

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, round, sum}
import org.apache.spark.sql.types.IntegerType

object Aggregator {


  def agg(df: DataFrame, colName: String, alias: String): DataFrame = {

    val groupCols = df.columns.diff(Seq(colName))
    df.groupBy(groupCols.map(col): _*)
      .agg(round(sum(colName)).alias(alias))
  }
}
