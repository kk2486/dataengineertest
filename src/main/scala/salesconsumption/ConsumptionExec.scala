package salesconsumption

import org.apache.spark.sql.{DataFrame, SparkSession}

//  DF read , join and call ops method for aggregation

object ConsumptionExec {

  def performExecution(inputLocation: String, outputLocation: String, spark: SparkSession) = {


    val calendarDF = CSVReader.read(inputLocation, "/calendar.csv", spark)
    val salesDF = CSVReader.read(inputLocation, "/sales.csv", spark)
    val productDF = CSVReader.read(inputLocation, "/product.csv", spark)
    val storeDF = CSVReader.read(inputLocation, "/store.csv", spark)

    // joining with other df's and dropping unnecessary
    val joinedDF = salesDF
      .join(calendarDF, salesDF("dateId") === calendarDF("datekey"), "left")
      .join(productDF, salesDF("productId") === productDF("productid"), "left").drop("productid")
      .join(storeDF, salesDF("storeId") === storeDF("storeid"), "left").drop("storeid")

    //call ConsumptionOperator for all operations

    val finalCosumptionDS = ConsumptionOps.operate(joinedDF, spark)

    //write the dataframe with single partition as Json
    finalCosumptionDS.toDF
  }

  def write(df: DataFrame, outputLocation: String) = {

    // I need to think a bit how i can write different file per row in DF.
    // I'm sure it is possible basing on getting count of lines and doing repartition
    // once we have the file(s), if there is requirement to rename the file. Using hadoop utils can be easily done.

    df.repartition(1).write.mode("overwrite").json(outputLocation + "/consumption")


  }

}
