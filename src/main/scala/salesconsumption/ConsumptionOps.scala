package salesconsumption

import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions._
import utils.{Consumption, DataRows}

// performs aggregation and convering to final Case Classes

object ConsumptionOps {

  def operate(joinedDF: DataFrame, spark: SparkSession): Dataset[Consumption] = {

    // found some spaces, to remove them
    val trimmedDF = joinedDF.columns.foldLeft(joinedDF)(
      (df, col) => df.withColumn(col, trim(df(col)))
    )

    // preparing weekID, renaming year,dropping unncessary

    val selectedValsDF = trimmedDF.withColumn("weekId", concat(lit("W"), col("weeknumberofseason")))
      .withColumnRenamed("datecalendaryear", "year")
      .withColumn("uniqueKey", concat(col("year"), lit("_"), col("channel"), lit("_"), col("division"), lit("_"), col("gender"), lit("_"), col("category")))
      .drop("weeknumberofseason", "datecalendarday", "dateId", "country", "saleId", "datekey", "productid", "storeid")
      .select("uniqueKey", "division", "gender", "category", "channel", "year", "weekId", "netSales", "salesUnits")

    // finding aggregate for dataframe on netSales, Sales Units

    val aggregatedDF = Aggregator.agg(Aggregator.agg(selectedValsDF, "netSales", "Net Sales"), "salesUnits", "Sales Units")

    import spark.implicits._

    val consumptionDF = aggregatedDF.map(r => {

      val uniqueKey = r.getString(0)
      val division = r.getString(1)
      val gender = r.getString(2)
      val category = r.getString(3)
      val channel = r.getString(4)
      val year = r.getString(5)
      val weekId = r.getString(6)

      val netSales = r.getDouble(7).toInt
      val salesUnits = r.getDouble(8).toInt

      val mapValNetSales = Map(weekId -> netSales)
      val mapValSalesUnits = Map(weekId -> salesUnits)

      val mapDefault = (1 to 52).map { e => ("W" + e.toString, 0) }.toMap

      val mapFinalNetSales = mapValNetSales ++ mapDefault.map { case (k, v) => k -> (v + mapValNetSales.getOrElse(k, 0)) }
      val mapFinalSalesUnits = mapValSalesUnits ++ mapDefault.map { case (k, v) => k -> (v + mapValSalesUnits.getOrElse(k, 0)) }

      val seqDataRows = Seq(DataRows("Net Sales", mapFinalNetSales), DataRows("Sales Units", mapFinalSalesUnits))

      Consumption(uniqueKey, division, gender, category, channel, year, seqDataRows)
    }
    )
    consumptionDF

  }

}
