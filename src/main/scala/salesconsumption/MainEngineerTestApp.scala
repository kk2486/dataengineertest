package salesconsumption

import org.apache.spark.sql.SparkSession


/**
 * Spark Main app to calculate Aggregate values of
 * Sales.csv by joining with calendar.csv,product.csv
 * & store.csv
 *
 *
 *
 * */

object MainEngineerTestApp extends App {

  val spark = SparkSession
    .builder
    .appName("dataengineertest")
    .getOrCreate()

  if (args.length < 2 || args.length > 2) {
    //TODO : can be changed to a logger
    println("No proper arguments passed")

    System.exit(0)

  }
  val inputLocation = args(0)
  val outputLocation = args(1)

  val finalDF = ConsumptionExec.performExecution(inputLocation, outputLocation, spark)

  ConsumptionExec.write(finalDF, outputLocation)
}
