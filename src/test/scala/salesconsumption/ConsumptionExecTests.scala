package salesconsumption

import org.apache.hadoop.fs.FileUtil
import org.apache.spark.sql.SparkSession
import java.io.File
import org.scalatest.{BeforeAndAfterEach}
import org.scalatest.concurrent.Eventually
import org.scalatest.funsuite.AnyFunSuite

// Tests
//TODO: basic tests, can be changed further by adding UT, IT seperately.

class ConsumptionExecTests extends AnyFunSuite with Eventually with BeforeAndAfterEach {

  val inputLocation = "src/test/resources/input"
  val outputLocation = "src/test/resources/output"


  val spark = SparkSession.builder
    .master("local[*]")
    .appName("Test(s)")
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")

  override def beforeEach() = FileUtil.fullyDeleteContents(new File(outputLocation))

  override def afterEach() = FileUtil.fullyDeleteContents(new File(outputLocation))


  test("Aggregated DF count") {

    val df = ConsumptionExec.performExecution(inputLocation, outputLocation, spark)

    assert(df.count === 10)
  }

  test("Aggregated  Json Read and Count Check") {

    val df = ConsumptionExec.performExecution(inputLocation, outputLocation, spark)

    ConsumptionExec.write(df, outputLocation)

    val jsonDF = spark.read.option("primitivesAsString", "true").json(outputLocation + "/consumption")

    assert(jsonDF.count === 10)
  }


}
