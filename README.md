# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is Data engineer test package provided
 * Version -- 0.1

### How to run? ###

* Execute the Application
```
spark-submit --class salesconsumption.MainTestApp s3://consumptionsalesappjars/dataengineertest-assembly-0.1.jar s3a://consumptionsalessource s3a://consumptionsalestarget
```
In simple
```
spark-submit --class {mainApp} <jarname> <argument1> <arguement2>

 {mainApp} --> salesconsumption.MainTestApp
 {jarname} --> Generated Jar name ( Available in target directory)
 {argument1} --> s3://consumptionsalessource
 {argument1} --> s3a://consumptionsalestarget
 
 ```





* Dependencies
```  Added in build.sbt```
* How to run tests
  <code> sbt test </code>
* Test Result in EMR(optional)
  ![Image of EMR Test](src/test/resources/EMR.jpg)

###### Note: docs for methods, generic comments are not provided due to time constraint
